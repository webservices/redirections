# Webservices-Redirections

This is a project to redirect existing sites to a new location temporarily. It uses nginx s2i that could be found under the Openshift Catalog.

## Procedure

Use case:

- I want to redirect `medical-service.web.cern.ch` to `hse.cern/services-support/occupational-health-service`.

First create the proper route you want to redirect.

```bash
oc create route edge <sitename> --service=webservices-redirections --hostname='<sitename>.web.cern.ch' --insecure-policy='Redirect'
# e.g.: oc create route edge medical-service --service=webservices-redirections --hostname='medical-service.web.cern.ch' --insecure-policy='Redirect'
```

Second, update the `nginx.conf` file accordingly. This will automatically deploy new changes to the project through GitLab CI.

```bash
# E.g.:
...
if ($host = "medical-service.web.cern.ch") {
    return 301 https://hse.cern/services-support/occupational-health-service;
    break;
}
...
```

## Deploy

In order to deploy changes, there are two projects for this:

- test-webservices-redirections.web.cern.ch (under [openshift-dev.cern.ch](https://openshift-dev.cern.ch)).
- webservices-redirections.web.cern.ch (under [openshift.cern.ch](https://openshift.cern.ch)).

Changes on `dev` branch go directly to `test-webservices-redirections.web.cern.ch`.

When happy with the changes, merge them all into `master` branch. This won't automatically deploy to `webservices-redirections.web.cern.ch`, but you need to explictly allow the JOB to be triggered.